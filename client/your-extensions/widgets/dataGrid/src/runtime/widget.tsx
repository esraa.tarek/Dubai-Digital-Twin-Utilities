import { useState, useEffect } from 'react'
import * as signalR from '@microsoft/signalr'

import { React, AllWidgetProps } from 'jimu-core'
import { IMConfig } from '../config'
import zoomBtnRenderer from '../../../zoomBtnRenderer/src/runtime/widget'

import * as AgGridReactLib from 'ag-grid-react'

import 'ag-grid-community/styles//ag-grid.css'
import 'ag-grid-community/styles//ag-theme-alpine.css'
import './widget.scss'

const Widget = ({objId, data, jimuMapView}) => {
  const {AgGridReact} = AgGridReactLib;

  const [gridData, setGridData] = useState<object[]>([])

  useEffect(() => {
    if (data) {
      setGridData([data, ...gridData])
    }
  }, [data])

  const columnDefs = [
    { headerName: 'id', field: 'id', width: 90 },
    { headerName: 'Record Id', field: 'recordId', width: 130 },
    { headerName: 'Customer Name', field: 'customerName', width: 160 },
    { headerName: 'Customer Number', field: 'customerNumber', width: 160 },
    { headerName: 'Field Name', field: 'fieldName', width: 250 },
    { headerName: 'Value', field: 'value', width: 100 },
    { headerName: 'Message', field: 'message', width: 250 },
    {
      headerName: '',
      field: 'geometry',
      width: 60,
      cellRenderer: zoomBtnRenderer,
      cellRendererParams: {
        jimuMapView: jimuMapView,
        clicked: function (field) {
          alert(`${field} was clicked`)
        }
      }
    }
    // {headerName: "Table Name", field: "tableName"},
    // {headerName: "Is Read", field: "isRead"},
  ]

  // const rowData 
  // = [
  //   {
  //     customerName: 'Esraa',
  //     customerNumber: null,
  //     fieldName: "Current_Consumption",
  //     id: 8883,
  //     isRead: false,
  //     message: "Current_Consumption",
  //     recordId: 9,
  //     tableName: "Gas_Meter",
  //     value: 0.008
  //   },
  //   {
  //     customerName: 'Linda',
  //     customerNumber: null,
  //     fieldName: "Current_Consumption",
  //     id: 8883,
  //     isRead: false,
  //     message: "Current_Consumption",
  //     recordId: 9,
  //     tableName: "Gas_Meter",
  //     value: 0.008
  //   },
  //   {
  //     customerName: 'Nermeen',
  //     customerNumber: null,
  //     fieldName: "Current_Consumption",
  //     id: 8883,
  //     isRead: false,
  //     message: "Current_Consumption",
  //     recordId: 9,
  //     tableName: "Gas_Meter",
  //     value: 0.008
  //   },
  // ];

  return (
    <div className="widget-demo jimu-widget m-2 ag-theme-alpine">
      <AgGridReact
      columnDefs={columnDefs}
      rowData={gridData}
      alwaysShowHorizontalScroll={true}
      alwaysShowVerticalScroll={true}
      pagination={true}
      sortingOrder={['desc']}
      />
    </div>
  )
}

export default Widget
