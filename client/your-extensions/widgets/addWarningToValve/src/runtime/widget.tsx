import { useState, useEffect } from 'react'
import * as signalR from '@microsoft/signalr'

import { React, AllWidgetProps } from 'jimu-core'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis'

import GraphicsLayer from 'esri/layers/GraphicsLayer'
import FeatureLayer from 'esri/layers/FeatureLayer'
import Graphic from 'esri/Graphic'
import SpatialReference from 'esri/geometry/SpatialReference'

import { IMConfig } from '../config'

import DataGrid from '../../../dataGrid/src/runtime/widget'
import { Geometry } from 'esri/geometry'

import './widget.scss'

const Widget = (props: AllWidgetProps<IMConfig>) => {
  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()
  const [objId, setObjId] = useState<string>(null)
  const [dBId, setId] = useState<string>(null)
  const [graphicsLayer, setGraphicsLayer] = useState<GraphicsLayer>()
  const [geometry, setGeometry] = useState<object>(null)
  const [socketData, setSocketData] = useState<object>(null)
  const [allSocketDataObj, setAllSocketDataObj] = useState<object>({})
  const [allSocketDataWithGeometryObj, setAllSocketDataWithGeometryObj] = useState<object>({})

  useEffect(() => {
    const connection = new signalR.HubConnectionBuilder().withUrl('https://digitalcatalogue.eastus.cloudapp.azure.com/DigitalTwin/notificationHub', {
      skipNegotiation: true,
      transport: signalR.HttpTransportType.WebSockets
    }).withAutomaticReconnect().build()

    connection.start().then(() => {
    }).catch((e) => {
      console.log("signal R connection error!")
      console.log(e)
    })

    connection.on('SendNotification', data => {
      console.log('SendNotification', data)
      const objId = data.recordId
      setObjId(objId)
      setId(data.id)
      // setSocketData(data);

      //save all incoming notifications in object
      setAllSocketDataObj({ [data.id]: data, ...allSocketDataObj })
    })
  }, [])

  useEffect(() => {
    if (dBId) { addWarningIndicator(objId) }
  }, [dBId])

  useEffect(() => {
    if (geometry && geometry[dBId]) { 
      setAllSocketDataWithGeometryObj({
        [dBId]: {...allSocketDataObj[dBId], geometry: geometry[dBId]},
         ...allSocketDataWithGeometryObj
        })
      // setObjId(null)
      setGeometry(null)
    }
  }, [geometry])

  useEffect(() => {
    if (jimuMapView) {
      jimuMapView.view.when(() => {
        const graphicsLayer = new GraphicsLayer(
          {
            id: 'Warning',
            title: 'Warning',
            elevationInfo: { mode: 'relative-to-scene' } //offset: -6
          }
        )
        jimuMapView.view.map.add(graphicsLayer)
        setGraphicsLayer(graphicsLayer)
      })
    }
  }, [jimuMapView])

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  const addWarningIndicator = (objId: string) => {
    console.log(jimuMapView)

    if (jimuMapView?.view) {
      // const gasPipeFL = new FeatureLayer({ url: 'https://trdvisibility.eastus.cloudapp.azure.com/server/rest/services/DubaiDigitalTwinDemo/DubaiDigitalTwin_Gas_Pipes/FeatureServer/1' })
      // const gasMeterFL = new FeatureLayer({ url: 'https://trdvisibility.eastus.cloudapp.azure.com/server/rest/services/DubaiDigitalTwinDemo/DubaiDigitalTwin_Gas_meter/FeatureServer/1' })
      // const sanitaryManholeFL = new FeatureLayer({ url: 'https://services1.arcgis.com/5Go75syq3JlGJLEe/ArcGIS/rest/services/Dubai_Digital_Twin_WSL14/FeatureServer/1' })
      // const gasMeter2DFL = new FeatureLayer({ url: 'https://trdvisibility.eastus.cloudapp.azure.com/server/rest/services/GasMeter_For_Updating/FeatureServer/0' })
      // const gasMeterOldFL = new FeatureLayer({ url: 'https://services1.arcgis.com/5Go75syq3JlGJLEe/ArcGIS/rest/services/Dubai_Digital_Twin_WSL8/FeatureServer/1' })
      // const gasPipesOldFL = new FeatureLayer({ url: 'https://services1.arcgis.com/5Go75syq3JlGJLEe/ArcGIS/rest/services/Dubai_Digital_Twin_WSL7/FeatureServer/1' })
      // const gasMeterDataFL = new FeatureLayer({ url: 'https://trdvisibility.eastus.cloudapp.azure.com/server/rest/services/Hosted/Gas_meter_Data/FeatureServer/0' })
      const gasMeterPointFL = new FeatureLayer({ url: 'https://trdvisibility.eastus.cloudapp.azure.com/server/rest/services/DubaiDigitalTwinDemo/Gas_Meter_Pnt/FeatureServer/0' })
      // jimuMapView.view.layerViews._items[0].layerViews._items[1].layer
      // jimuMapView.view.layerViews.items[2].layer.layers.items[2]
      // jimuMapView.view.layerViews.items[2].layerViews.items[2]
      // gasPipesOldFL
      gasMeterPointFL
        .queryFeatures({ where: 'OBJECTID_1=' + objId, returnGeometry: true, outFields: '*' })
        .then((data) => {
          console.log(data.features[0].geometry)

          //update geometry for this objId
          setGeometry({[dBId]: data.features[0].geometry});

          const point = {
            type: 'point', // autocasts as new Point()
            x: data.features[0].geometry.x,
            y: data.features[0].geometry.y,
            // z: 1010,
            // spatialReference: SpatialReference.WebMercator
            spatialReference: data.features[0].geometry.spatialReference
          }

          // verticalOffset shifts the symbol vertically
          const verticalOffset = {
            screenLength: 2,
            maxWorldLength: 200,
            minWorldLength: 10
          }

          const markerSymbol = {
            type: 'point-3d', // autocasts as new PointSymbol3D()
            symbolLayers: [
              {
                type: 'icon', // autocasts as new IconSymbol3DLayer()
                resource: {
                  href: 'https://img.icons8.com/emoji/48/warning-emoji.png'
                },
                size: 20,
                outline: {
                  color: 'white',
                  size: 2
                }
              }
            ]
            // ,
            // verticalOffset: verticalOffset,
            // callout: {
            //   type: 'line', // autocasts as new LineCallout3D()
            //   color: 'white',
            //   size: 2,
            //   border: {
            //     color: 'red'
            //   }
            // }
          }
          // {
          //   type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
          //   color: [255, 0, 0],
          //   outline: {
          //   // autocasts as new SimpleLineSymbol()
          //     color: [255, 0, 0],
          //     width: 50
          //   }
          // }

          const warningGaphic = new Graphic({
            geometry: data.features[0].geometry, //.centroid, //point
            symbol: markerSymbol
          })

          const currentElevationInfo = {
            mode: 'relative-to-ground',
            offset: 0,
            featureExpressionInfo: {
              expression: data.features[0].attributes.Placement_Elev
            },
            unit: 'meters'
          }

          graphicsLayer.elevationInfo = currentElevationInfo
          graphicsLayer.add(warningGaphic)

          // jimuMapView.view.goTo(
          //   data.features[0].geometry //.centroid //point
          // )

          // setGeometry(data.features[0].geometry)
          // setSocketData({...socketData, geometry: data.features[0].geometry})

          jimuMapView.view.goTo({
            center: data.features[0].geometry, //.centroid,
            zoom: 21
          })
          // data.features[0].geometry.rings[0][0] //first point in the manhole geometry
        })
        .catch((err) => {
          console.log(err)
        })
    }

    // setSocketData(null)
  }

  return (
    <div className="valve-warning widget-demo jimu-widget">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      <span className='valve-warning-title'>Alerts</span>
      <DataGrid objId={dBId} data={allSocketDataWithGeometryObj[dBId]} jimuMapView={jimuMapView} />
    </div>
  )
}

export default Widget
