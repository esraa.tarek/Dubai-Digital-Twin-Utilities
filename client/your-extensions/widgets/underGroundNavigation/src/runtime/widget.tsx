import { React, AllWidgetProps } from 'jimu-core'
import { IMConfig } from '../config'
import { useEffect, useState } from 'react'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis'

const Widget = (props: AllWidgetProps<IMConfig>) => {
  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()
  const [isUnderGround, setIsUnderGround] = useState<Boolean>(false)

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  const onGoUndergroundClick = () => {
    if (jimuMapView) {
      jimuMapView.view.goTo(jimuMapView.view.map.presentation.slides.getItemAt(2).viewpoint, {
        duration: 1000
      }).catch((error) => {
        if (error.name !== 'AbortError') {
          console.error(error)
        }
      })
      setIsUnderGround(true)
    }
  }

  const onAboveGroundClick = () => {
    if (jimuMapView) {
      jimuMapView.view.goTo(jimuMapView.view.map.presentation.slides.getItemAt(1).viewpoint, {
        duration: 1000
      }).catch((error) => {
        if (error.name !== 'AbortError') {
          console.error(error)
        }
      })
      setIsUnderGround(false)
    }
  }

  useEffect(() => {
    if (jimuMapView) {
      jimuMapView.view.when(() => {
        // allow navigation above and below the ground
        jimuMapView.view.map.ground.navigationConstraint = {
          type: 'none'
        }
        // the webscene has no basemap, so set a surfaceColor on the ground
        jimuMapView.view.map.ground.surfaceColor = '#fff'
        // to see through the ground, set the ground opacity to 0.4
        jimuMapView.view.map.ground.opacity = 0.4
      })
    }
  }, [jimuMapView])

  const bgColor = { backgroundColor: '#076fe5', borderRadius: '5px', color: '#fff', borderColor: 'transparent', padding: '4px 6px' }

  return (
    <div className="widget-demo jimu-widget m-2">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      {!isUnderGround
        ? <button style={bgColor} onClick={onGoUndergroundClick}>Go under ground</button>
        : <button style={bgColor} onClick={onAboveGroundClick}>Go above ground</button>
      }
    </div>
  )
}

export default Widget
