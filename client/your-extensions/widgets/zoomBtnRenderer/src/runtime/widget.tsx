import { React, AllWidgetProps } from 'jimu-core'
import { IMConfig } from '../config'

import ZoomIcon from './zoom-icon.svg'
import './widget.scss'

const Widget = (props: AllWidgetProps<IMConfig>) => {

  const zoomClickedHandler = () => {
    console.log(props.value); //geometry
    props.jimuMapView.view.goTo({
      center: props.value,
      zoom: 21
    })
  }

  return (
    <button className='zoom-renderer_btn' onClick={zoomClickedHandler}><span>
      <img className='zoom-renderer_icon' src={ZoomIcon} />
      </span></button>
  )
}

export default Widget
